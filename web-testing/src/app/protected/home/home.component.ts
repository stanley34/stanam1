import {Component, ElementRef, OnInit} from '@angular/core';
import { finalize } from 'rxjs/operators';

import { QuoteService } from './quote.service';

interface TableButtonGroup {
  title: string,
  color: string
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  quote: string;
  isLoading: boolean;

  constructor(private quoteService: QuoteService) { }

  ngOnInit() {
    this.isLoading = true;
    this.quoteService.getRandomQuote({ category: 'dev' })
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe((quote: string) => { this.quote = quote; });

    let rows: (string | number | TableButtonGroup[] | ElementRef)[] = ["female", 12, "text"]

  }

}
