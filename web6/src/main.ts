import './polyfills';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import '../public/styles/styles.scss';
platformBrowserDynamic().bootstrapModule(AppModule);