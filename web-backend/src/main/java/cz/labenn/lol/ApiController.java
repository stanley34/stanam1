package cz.labenn.lol;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequestMapping("/api")
@RestController
public abstract class ApiController {
    private static final Logger LOGGER = Logger.getLogger(ApiController.class);


    @GetMapping(value = "/value/",produces = APPLICATION_JSON_VALUE)
    public ResponseEntity getListHandle() {
        return ResponseEntity.ok().build();
    }

}
