package cz.labenn.lol.testingEnviroment;

import org.junit.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TestingEnv {
    /*
     *  * Start
     *  ** Log in
     *  ** Move to page
     *  * Change config
     *  ** config 1
     *  ** config 2
     *  *** config 2 easy
     *  *** config 2 hard
     *  ** config #
     *  * Do stuff
    **/

    @Test
    public void FirstTest() {
        Indent[] indents = {
                new Indent("*", "Start"),
                new Indent("**", "Log in"),
                new Indent("**", "Move to page"),
                new Indent("*", "Change config"),
                new Indent("*#", "Config 1"),
                new Indent("*#", "Config 2"),
                new Indent("#", "Config 2 easy"),
                new Indent("*#*", "Config 2 hard"),
                new Indent("*#", "Config 3"),
                new Indent("*", "Do stuff"),
                new Indent("***", "Skipped level 2"),
                new Indent("", "Text outside ")
        };

        ArrayList<Element> listLevels = new ArrayList<>();
        for (Indent indent : indents) {
            char[] specialCharacters = indent.getLevel().toCharArray();
            Element parent = null;
            for (int i = 0; i < specialCharacters.length; i++) {
                if ((specialCharacters[i] == '*' && listLevels.get(i) instanceof UnorderedList) ||
                        (specialCharacters[i] == '#' && listLevels.get(i) instanceof OrderedList)) {

                } else {
                    if (listLevels.get(i) != null) {
                        Integer finalI = i;
                        listLevels.subList(i, listLevels.size()).clear();

                    }
                    listLevels.add(new OrderedList());
                }
            }
            if (listLevels.get(indent.getLevel().length()) != null) {

            }
        }

    }

    public Map<Integer, Element> filtered(Map<Integer, Element> map, Integer higherThan) {
        return map.entrySet().stream()
                .filter(entry -> entry.getKey()>= higherThan)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @Test
    public void testMethod() {
        String text = "*** Text";
        String pattern = "^[*#]+";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);
        if (m.find( )) {
            System.out.println("Found value: " + m.group(0) );
        }else {
            System.out.println("NO MATCH");
        }
    }
}
