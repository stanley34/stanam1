import { AppComponent }  from './app.component';
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ChartModule} from "angular-highcharts";
import {ChartComponent} from "../chart/chart.component";

@NgModule({
    imports: [
        BrowserModule,
        NgbModule,
        ChartModule
    ],
    declarations: [
        AppComponent,
        ChartComponent
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }