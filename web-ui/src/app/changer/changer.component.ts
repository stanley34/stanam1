import { Component, OnInit } from '@angular/core';
import {ModelService} from "../model.service";

@Component({
  selector: 'app-changer',
  templateUrl: './changer.component.html',
  styleUrls: ['./changer.component.css']
})
export class ChangerComponent implements OnInit {

  constructor(private _modelService: ModelService) { }

  ngOnInit() {
  }

  change(){
    this._modelService.changeValue();
  }

  get(){
    return this._modelService.get()
  }
}
