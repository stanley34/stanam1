package cz.labenn.lol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;


@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:application.properties" })
public class AppConfig {
    @Autowired
    Environment enviroment;


    @Value("${connection.url}")
    String connectionURL;

}

