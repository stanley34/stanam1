package cz.labenn.lol.testingEnviroment;

import java.lang.reflect.Array;

public class TestClass<T extends Comparable<T>> {

    private final Class<T> clazz;

    TestClass(Class<T> clazz) {
        this.clazz = clazz;
    }


    public T[] mergeArrays(T[] arr1, T[] arr2) {

        T[] arr3 = (T[]) Array.newInstance(clazz, arr1.length + arr2.length);
        int i = 0, j = 0, k = 0;
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i].compareTo(arr2[j]) < 0) {
                arr3[k] = arr1[i];
                i++;
            } else {
                arr3[k] = arr2[j];
                j++;
            }
            k++;
        }

        if (i < arr1.length) {
            arr3 = mergerRestOfArray(arr3, arr1, i, k);
        }

        if (j < arr2.length) {
            arr3 = mergerRestOfArray(arr3, arr2, j, k);
        }
        return arr3;
    }

    private T[] mergerRestOfArray(T[] mergedArray, T[] partArray, int i, int k) {
       while (i < partArray.length) {
           mergedArray[k] = partArray[i];
           i++;
           k++;
       }
       return mergedArray;
    }
}
