import {Component, ContentChild, Input, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './app-tab.component.html',
  styleUrls: ['./app-tab.component.scss']
})
export class AppTabComponent implements OnInit {

  @Input() childTemplate: TemplateRef<any>;

  myContext = {$implicit: 'World', localSk: 'Svet'};

  ctx: {
    message: 'TEXT'
  };

  value = false;
  constructor() { }

  ngOnInit() {
  }

}
