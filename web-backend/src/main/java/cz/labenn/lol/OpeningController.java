package cz.labenn.lol;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class OpeningController extends ApiController{

    @GetMapping(value = "/opening/{opening}",produces = APPLICATION_JSON_VALUE)
    public ResponseEntity getHandle(@PathVariable String opening) {
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/opening/",produces = APPLICATION_JSON_VALUE)
    public ResponseEntity getListHandle() {
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/opening", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity saveHandle(){
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/opening/{opening}")
    public ResponseEntity deleteHandle(@PathVariable String opening) {
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/opening", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity insertHandle() {
        return ResponseEntity.ok().build();
    }
}
