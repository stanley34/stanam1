package cz.labenn.lol.testingEnviroment;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

class Wrapper {
    private Integer level;
    private Element listType;

    public Wrapper(Integer level, Element listType) {
        this.level = level;
        this.listType = listType;
    }
}

class Indent {
    private String level;
    private String text;

    public Indent(String level, String text) {
        this.level = level;
        this.text = text;
    }

    public String getLevel() {
        return level;
    }

    public String getText() {
        return text;
    }
}

class HtmlBuilder {
    private StringBuilder htmlString;
    private Deque<String> endTags;

    HtmlBuilder() {
        htmlString = new StringBuilder();
        endTags = new ArrayDeque<>();
    }

    void addSpan(String text) {
        htmlString.append("<span>")
                .append(text)
                .append("</span>");
    }

    String toHtmlString() {
        endTags.descendingIterator().forEachRemaining(htmlString::append);
        return htmlString.toString();
    }

    void startLi() {
        htmlString.append("<li>");
        endTags.push("</li>");
    }

    void endLastOpenElement(){
        htmlString.append(endTags.pop());
    }

    void addElementBody(String string) {
        htmlString.append(string);
    }

    public void startOl() {
        htmlString.append("<ol>");
        endTags.push("</ol>");
    }

    public void startUl() {
        htmlString.append("<ul>");
        endTags.push("</ul>");
    }
}

abstract class Element {
    HtmlBuilder htmlBuilder;

    abstract String toHtmlString();
}

class OrderedList extends Element {
    private List<Element> childElements;

    public OrderedList() {
        this.htmlBuilder = new HtmlBuilder();
        childElements = new LinkedList<>();
        this.htmlBuilder.startOl();
    }

    void addChildElement(Element element) {
        childElements.add(element);
    }

    @Override
    public String toHtmlString() {
        childElements.forEach(element -> this.htmlBuilder.addElementBody(element.toHtmlString()));
        this.htmlBuilder.endLastOpenElement();
        return this.htmlBuilder.toHtmlString();
    }
}

class UnorderedList extends Element {
    private List<Element> childElements;

    public UnorderedList() {
        this.htmlBuilder = new HtmlBuilder();
        childElements = new LinkedList<>();
        this.htmlBuilder.startUl();
    }

    void addChildElement(Element element) {
        childElements.add(element);
    }

    @Override
    public String toHtmlString() {
        childElements.forEach(element -> this.htmlBuilder.addElementBody(element.toHtmlString()));
        this.htmlBuilder.endLastOpenElement();
        return this.htmlBuilder.toHtmlString();
    }
}

class ListItem extends Element {
    private Element parent;

    public ListItem(String text) {
        this.htmlBuilder = new HtmlBuilder();
        this.htmlBuilder.startLi();
        this.htmlBuilder.addSpan(text);
    }


    public ListItem() {
        this.htmlBuilder = new HtmlBuilder();
        this.htmlBuilder.startLi();
    }


    @Override
    public String toHtmlString() {
        return this.htmlBuilder.toHtmlString();
    }
}