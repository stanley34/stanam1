import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoaderComponent } from './loader/loader.component';
import {GenericTableComponent} from "@app/shared/component/generic-table/generic-table.component";
import { AppTabComponent } from './component/app-tab/app-tab.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoaderComponent,
    GenericTableComponent,
    AppTabComponent
  ],
  exports: [
    LoaderComponent,
    GenericTableComponent,
    AppTabComponent
  ]
})
export class SharedModule { }
