import { Injectable } from '@angular/core';

@Injectable()
export class ModelService {
  private value: boolean = false;

  constructor() { }

  changeValue(){
    this.value = !this.value;
  }

  get() {
    return this.value;
  }
}
