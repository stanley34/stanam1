"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("./polyfills");
const platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
const app_module_1 = require("./app/app.module");
require("../public/styles/styles.scss");
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule);
//# sourceMappingURL=main.js.map