import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InfoPanelComponent } from './json-compact/info.panel.component';
import { ChangerComponent } from './changer/changer.component';
import {ModelService} from "./model.service";

@NgModule({
  declarations: [
    AppComponent,
    InfoPanelComponent,
    ChangerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ModelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
